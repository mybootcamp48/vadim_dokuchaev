# Generated by Django 3.0 on 2019-12-15 12:31

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Human',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='pages/media', verbose_name='Фото человека')),
                ('text', models.TextField(verbose_name='Описание')),
                ('link', models.CharField(max_length=100, verbose_name='Ссылка на страницу человека')),
            ],
            options={
                'verbose_name': 'Человек',
                'verbose_name_plural': 'Люди',
            },
        ),
    ]
