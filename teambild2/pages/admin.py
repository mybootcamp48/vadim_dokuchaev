from django.contrib import admin
from pages.models import Human


@admin.register(Human)
class HumanAdmin(admin.ModelAdmin):
    list_display = ['id']
