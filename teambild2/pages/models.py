from django.db import models
from django.urls import reverse


class Human(models.Model):
    image = models.ImageField('Фото человека', upload_to='../media/')
    text = models.TextField('Описание')
    link = models.CharField('Ссылка на страницу человека', max_length=100)

    def __int__(self):
        return self.id

    def get_absolute_url(self):
        return reverse('index', kwargs={'pk': self.id})

    class Meta:
        verbose_name_plural = 'Люди'
        verbose_name = 'Человек'
