from django.shortcuts import render
from pages.models import Human


def index(request):
    if request.method == 'GET':
        humans = Human.objects.all()
        conext = {'humans': humans}
        return render(request, 'main.html', conext)
